﻿#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int countVowels(const string& word) {
    int count = 0;
    for (char c : word) {
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' || c == 'Y') {
            count++;
        }
    }
    return count;
}

bool compareByVowelRatio(const pair<string, double>& a, const pair<string, double>& b) {
    return a.second < b.second;
}

int main() {
    string text;
    cout << "Enter text in English (max 256 characters): ";
    getline(cin, text, '\n');
    text = text.substr(0, 256);

    const int MAX_WORDS = 1024;
    string words[MAX_WORDS];
    int wordCount = 0;
    string word;
    for (char c : text) {
        if (c == ' ') {
            if (!word.empty()) {
                words[wordCount++] = word;
                word.clear();
            }
        }
        else {
            word += c;
        }
    }
    if (!word.empty()) {
        words[wordCount++] = word;
    }

    pair<string, double> wordVowelRatios[MAX_WORDS];
    for (int i = 0; i < wordCount; i++) {
        int totalChars = static_cast<int>(words[i].length());
        int vowelCount = countVowels(words[i]);
        double vowelRatio = static_cast<double>(vowelCount) / totalChars;
        wordVowelRatios[i] = { words[i], vowelRatio };
    }

    sort(wordVowelRatios, wordVowelRatios + wordCount, compareByVowelRatio);

    cout << "Original text:\n" << text << "\n\nWords sorted by increasing vowel percentage: \n";
    for (int i = 0; i < wordCount; i++) {
        cout << wordVowelRatios[i].first << " - " << wordVowelRatios[i].first << "\n";
    }

    return 0;
}